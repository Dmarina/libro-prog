# Programación Científica de Alto Rendimiento#

Obra destinada a la divulgación de varios lenguajes de programación.

Autores
-------

* Daniel Marín Aragón
* Rafa Rodríguez Galván
* [Edita el Readme y apunta tu nombre]

Estructura (provisional)
------------------------

* Parte 1. Introducción (...)
* Parte 2. Métodos numéricos
	* Conceptos matemáticos básicos:  Coma flotante (estándar IEEE), errores, teoremas y resultados fundamentales (valor medio, etc)
	* Teoría (...)
	* Ejemplos usando el código Python desarrollado en la parte 3
* Parte 3. Python
	* Implementación en Python de los algoritmos de la parte 2
* Parte 4. Cálculo científico de alto rendimiento
	* Conceptos básicos (Terminal Unix, Make, Git,...)
	* Fortran
	* C++
	* Paralelización


Licencia
--------

Esta obra está bajo una [Licencia Creative Commons Atribución 4.0 Internacional.](http://creativecommons.org/licenses/by/4.0/)


![alt tag](https://i.creativecommons.org/l/by/4.0/88x31.png)


1 de Septiembre de 2015
