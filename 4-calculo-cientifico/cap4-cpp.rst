.. _cap4-cpp:

=====================
 Programación en C++ 
=====================

En este capítulo, daremos algunas nociones de C++ con el objectivo de poder aplicar los métodos aprendidos
en la Sección [la que sea]. Debido la intención de este libro daremos un muy breve repaso a las nociones básaicas en C/C++. Se recomienda encarecidamente a los lectores que no conozcan dicho lenguaje que consulte [poner referencia].

¿Cómo comenzar?
================

Esto es un programa básico en C++::

	#include<iostream>
  	using namespace std;
	int main(void)
	{
		cout<<"Hola mundo"<<endl;
		return(0);
	}

Si lo ejecutamos, obtendremos por consola obtendremos el mensaje *Hola mundo*. Analicemos el código:

	* **#include<iostream>** y **using namespace std**: nos sirve para cargar la biblioteca
	  de entrada y salida, así como el espacio de nombres estandard. Para nuestro propósito,
	  siempre incluiremos ambos.
	* **int main(void)**: todo programa en C/C++ debe de estar dentro de una función *main*
	  para que se ejecute. Además, dicha función *main* debe de ser única en todo el programa.
	* **cout<<** y **<<endl**: sirve para escribir por la salida estandard y acabar la línea,
	  respectivamente.
	* **return(0)**: Devuelve *0* si no se ha producido ningún error en el programa. Para nuestro
	  propósito siempre acabaremos con esta sentencia.

Es importante destacar que toda sentencia en C/C++ tiene que acabar en ; para que no ocasione un error.

Tipos de variables
-------------------

En C/C++ encontramos los siguientes tipos de variables:

	* **bool:** Toma valores *verdadero* o *falso*.
	* **unsigned:** Toma valores enteros sin signo.
	* **int:** Toma valores enteros (8 bytes).
	* **float:** Toma valores reales (16 bytes).
	* **double:** Toma valores reales (32 bytes).
	* **char:** Almacena un caracter.
	* **FILE:** Sirve para almacenar la dirección de memoria de un fichero en memoria.

Existe un tipo especial de variable en C/C++ para almacenar direcciones de memoria que son los **punteros**.
Estos sirven para poder cambiar el valor de variables dentro de funciones. Se declaran con un *::

	#include<iostream>
  	using namespace std;
	void pruebaPuntero(int *a);
	int pruebaSinPuntero(int a);
	int main(void)
	{
		int a, *p;
		a=2;
		pruebaSinPuntero(a);
		cout<<"a="a<<endl;
		pruebaPuntero(&a);
		cout<<"a="a<<endl;
		return(0);
	}
	int pruebaSinPuntero(int a)
	{
		a++;
		return(a);
	}
	void pruebaPuntero(int *a)
	{
		*a++;
	}

Al compilar y ejecutar este programa obtenemos::
	
	a=2
	a=3

Ya que en la primera función no se está modificando el valor de la variable *a*. Para trabajar con punteros
se utilizan los operadores **&** (*dirección de memoria*) y * (*contenido*).

Notemos que cada variable puede almacenar un único valor. Si quisieramos almacenar más, necesitaríamos un **vector** o array. Estos puede almacenar un único tipo de variables, incluendo otros (matrices).
Más adelante hablaremos de vectores dinámicos, por ahora, para definir un vector lo haremos como en el siguiente ejemplo::

	char v[4];
	v[0]='s';
	v[1]='o';
	v[2]='l';
	v[3]='\0';

En este caso, hemos definido un vector que nos permite guardar tres caracteres más el caracter fin de cadena (solo necesario para las cadenas de caracteres). Para acceder a ellos usamos los operadores **[]**. Nótese que la primera posición corresponde al índice cero, y que tratar de acceder a una posición no reservada provocará un error en el programa.

Condicionales
--------------

Los condicionales en C/C++ usan las sentencias **if/else** o **switch/case**. Por ejemplo::

	if(<<condición>>)
	{
		<<Bloque de órdenes 1>>
	}
	else
	{
		<<Bloque de órdenes 2>>
	}

Si se verificara *condición* se ejecutaria el <<Bloque de órdenes 1>>, en caso contrario, el <<Bloque de órdenes 2>>. El otro comando, *switch*, se usa para elegir una opción entre varias posibilidades::

	switch(a):
	{
		case 1:
			<<Bloque de órdenes 1>>
			break;
		case 2:
			<<Bloque de órdenes 2>>
			break;
		default:
			<<Bloque de órdenes 3>>	
	}

En este caso, se analiza el valor de *a*, si es uno, se ejecuta <<Bloque de órdenes 1>>, si es dos, <<Bloque de órdenes 2>>, mientras que en cualquier otro caso, <<Bloque de órdenes 3>>. Es importante destacar que despues de cada caso se ha de añadir **break** con el fin de que no se ejecute el bloque de órdenes siguiente.

Bucles
------- 

En C/C++ existen tres tipos de bucles: **for**, **do/while** y **while**. Usaremos el primero cuando sabemos cuantas iteraciones tiene que realizar el bucle. Por ejemplo::

	for(i=0;i<5;i++)
	{
		v[i]=0;
	}

En este caso, iniciamos las 5 primeras posiciones del vector *v* a cero. El segundo tipo de bucle se usa para realizar una acción que luego deberá de repetirse mientras se verifique una condición::

	do
	{
		cout<<"Introduzca su edad"<<endl;
		cin<<edad;
	}while(edad<18);

El código anterior seguirá ejecutandose mientras el usuario no indique que es mayor que edad. Finalmente el último bucle se ejecutará mientras que se verifique una condición. Se diferencia del anterior en que primero verifica la condición y luego ejecuta el bloque de instrucciones::

	while(edad>18)
	{
		<<Bloque de instrucciones>>
	}

Nótese que en los bucles se pueden usar comandos como **break** o **continue**. Para más información consulte [poner referencia].

Ficheros
---------

Cuando un programa en C/C++ deja de ejecutarse, todos los datos sobre las variables se pierden. Si se desea conservar es necesario guardar dicha información en un fichero de texto. Un ejemplo básico del uso de ficheros es::

	FILE *fp;
	char c;	
	fp=fopen("datos.txt",r+);
	while((c=fgetc(fp)!=EOF))
	{
		cout<<c;
	}
	fclose(fp);

En este ejemplo, abrimos un fichero de texto llamado *datos.txt* y lo abrimos en modo lectura y escritura, a continuación leemos el fichero hasta llegar al caracter fin de fichero (**EOF**) y finalmente, cerramos el fichero.

Hay muchas más funciones, como **seek**, **rewind**... y más modos para abrir un fichero pero su explicación supondría excederse en la longitud de esta obra y por lo tanto, se remite al lector a [poner referencia].

Funciones
----------
En C/C++, como en otros lenguajes de programación, tales como Python, Fortran o Java, es recomendable escribir el código mendiante el uso de funciones. Estas son fragmentos de código que realizan un trabajo en concreto. Esto ayuda a simplificar el código en la función *main*, facilita la localización y corrección de errores... Una función en C/C++ tiene la siguiente estructura::

	<tipo a devolver> nombre (<tipo 1> parametro_1,<tipo 2> parametro_2)
	{
		<Código>
	}

Si  la función no devuelve nada, o no necesita parámetro usamos **void**. Para devolver un valor usamos **return**. Sólo podemos devolver una variable, por lo que si quisieramos devolver más, usaríamos un puntero a un bloque de memoria. Un ejemplo de función lo podemos ver en el apartado de Punteros::

	void pruebaPuntero(int *a);
	int pruebaSinPuntero(int a);

La primera función se llama *pruebaPuntero*, no devuelve nada y necesita un puntero a un entero como parámetro. La segunda, se llama *pruebaSinPuntero*, devuelve un entero y  toma otro como parámetro. Es importante volver a remarcar que los variables no se modifican dentro de una función y que si se desea hacerlo hay que usar punteros a dichas variables.

Cuando se crea una función se suele escribir el prototipo, es decir, la declaración de la función, (como en el ejemplo anterior) y luego el cuerpo de la función, que es la función propiamente dicha. Hay que hacer notar que el usuario no necesita, saber cómo realiza su labor una función, sino qué labor realiza y cuál es su prototipo para poder usarla.

Las variables que se crean en una función son locales, es decir, no pueden ser usadas fuera de la función, y viceversa, las creadas fuera, no pueden usarse dentro excepto si se pasan como parámetros.

Clases
-------

En ocasiones nos interesa ser capaces de encapsular varias variables y funciones bajo un único nombre, de tal manera que creando una variable de este tipo pudiéramos tener ya el resto de variables y funciones predefinidas. Nace asi el concepto de clase.

Una **clase** es una plantilla para crear variables formadas por otras variables y funciones. Un **objeto** es una variable de tipo clase. Veamos un ejemplo::

	class Complejo
	{
		friend Complejo operator+(const Complejo &izquierda, const Complejo &derecha);
		private:
			float real;
			float imaginario;
			void ceroComplejo{ real = 0.0; imaginario = 0.0; }
	
		public:
			Complejo();
			Complejo(float a, float b);
			void verComplejo(void);
			~Complejo();
	};

Empecemos a entender este código. Vamos a crear una variable que nos permita almacenar, números complejos y además operar con ellos. Para ello, usamos la palabra reservada **class** y a continuación escribimos el nombre que deseamos usar para la clase. En la siguiente línea *sobrecargamos* el operador +, con el fin de poder usarlo para nuestros complejos. A continuación, nos encontramos con **private** y **public**. Si una función o variable es privada, sólo podremos acceder a ella desde dentro de la clase, si fueran públicas, tambien podemos acceder a ella desde el exterior. En nuestro caso, hemos declarado como privadas, la parte real, la parte imaginaria y una función que nos inicializa las variables a cero. En la parte pública tenemos dos **constructores**, que nos permiten inicializar nuestro número complejo, así como una función para imprimirlo por pantalla. Finalmente, hemos incluido un **destructor**,  aunque al no estar trabajando con memoria dinámica, no es necesario.

Si no se domina alguno de los términos nombrados anteriormente, se recomienda consultar algunos de los referentes en la bibliografía, ya que van a ser usados a lo largo de la obra.

Para acceder a los campos de una clase usamos el operador punto (**.**). Por ejemplo si queremos imprimir por pantalla el número complejo *a*, usaríamos::

	a.verComplejo();

Para escribir el cuerpo de las funciones usamos **::** antes del nombre de la función, por ejemplo::

	Complejo operator+(const Complejo &izquierda, const Complejo &derecha)
	{
	Complejo aux;
	aux.real = izquierda.real + derecha.real;
	aux.imaginario = izquierda.imaginario + derecha.imaginario;
	return aux;
	}

.. Ejemplo de la clase "Complejo" basado en la clase "Complex":
.. Object Oriented C++ With Numerical Applications
.. Dr. Kieran F. Mulchrone
.. Department of Applied Mathematics,
.. University College,
.. Cork
.. September, 2008

