.. _cap4-unix:

=========================
Nociones básicas en Unix
=========================

Hoy día más del 90% de los super-ordenadores del mundo usan un sistema operativo derivado de Unix (ver www.top500.org). Esto se debe a la flexibilidad y a las múltiples ventajas que presenta este S.O. para trabajar en computación. Algunas de estas ventajas son su código abierto, el estar adaptado al cálculo en paralelo...  Por tanto, para completar esta obra daremos algunas pinceladas sobre el uso de la terminal de Unix, sobre la creación y uso de Makefiles...

La terminal de Unix
====================

La terminal o consola de Unix es un programa que ejecuta una shell que nos permite interactuar con el Sistema Operativo o con algún paquete de software por medio de comandos. A veces, se la suele llamar *interfaz de línea de comandos* (CLI, del inglés "command line interface") como oposición a la *interfaz gráfica de usuario* (GUI, del inglés "grafical user interface") que se basa en señalar con el ratón y clicar.

Si bien en Windows la opción más común es usar la GUI, también es posible abrir una terminal para su sistema operativo DOS. Nótese que DOS y UNIX son sistemas operativos distintos y por lo tanto, los comandos de uno no tienen que funcionar en el otro.

En un ordenador UNIX se suele trabajar en la terminal, aunque la mayoría de las versiones de Linux posee una GUI dependiendo del *administrador de ventanas*. En un Mac está también la opción de usar una shell de Unix en una terminal ya que el sistema operativo Mac OS X es esencialmente una versión de Unix.

Comandos básicos en Unix
-------------------------

Cuando abrimos una terminal (suele ser alt+ctrl+t), muestra un promt para indicar que está esperando una entrada por parte del usuario. En este libro, usaremos un $ para indicar el prompt, aunque esto es una cuestión de estética. En su ordenador puede elegir el que desee. Para usar un comando, escríbalo y puse *Enter*.

Hay muchos comandos en Unix y la mayoría de ellos tienen varios argumentos opcionales (generalmente especificados añadiendo algo como -x después del nombre del comando, donde x es un caracter). Aquí veremos sólamente los comandos básicos para poder empezar a trabajar. Para más información puede consultarse [referencia].

El comando **pwd** (del inglés "print working directory") escribe la ruta completa al directorio en el que se está trabajando actualmente. Un directorio es lo que se conoce habitualmente en Windows como *carpeta*. Un ejemplo del uso sería::

	$pwd
	/home/kirtash/MiBitbucket/libro-prog
	$

Podemos movernos entre los directorios usando el comando **cd** ("current directory"). Por ejemplo::

	$cd /home/kirtash

Para desplazarnos al directorio que acabamos de dejar usamos::

	$cd -

Y para subir un nivel en la jerarquía de los directorios::

	$cd ..

Para ver los archivos que tenemos en el directorio actual, usamos el comando **ls** ("list")

	$ls

Si queremos más información sobre los archivos usamos la opción **-l** que nos proporciona información sobre quien es el dueño, cuando se modificó la última vez, su tamaño...

Para una búsqueda más específica podemos usar el *carácter comodín*. Por ejemplo, para ver sólamente los ficheros de texto, haríamos la siguiente búsqueda::

	$ls *.txt

Aunque *ls* muestra la gran mayoría de archivos, algunos de ellos están "oculto", como por ejemplos aquellos que su nombre empiezan por punto como *.git* que es una carpeta con la configuración de git. Para ver este tipo de archivos usamos **-a**. Cuando usamos esta opción siempre aparecen dos directorios: **.** y **..** que se refieren al directorio actual y al directorio del nivel superior.

Cuando queramos saber más sobre un comando, qué función realiza o que opciones tiene, podemos consultar el manual. Por ejemplo, para ver la entra de *ls* escribiríamos::

	$man ls

Si haces esto, probablemente obtengas una página de información con **:** en la parte inferior. En este punto
estas en una shell diferente, una diseñada para permitirte desplazarte o buscar a través de un gran archivo con una terminal. Los **:** son el prompt de esta shell. Los comandos que puedes escribir en este punto son diferentes de los de la shell de Unix. Los más útiles son::

   : q [para salir de esta shell y volver a Unix]
   : <SPACE> [presiona la Barra Espaciadora para mostrar
              la siguiente pantalla completa]
   : b [vuelve a la pantalla completa anterior]

Podemos acceder a esta shell para visualizar nuestros propios archivos mediante el comando **less**, que muestra la primera pantalla completa del archivo y el prompt **:**. Para ver archivos podemos usar también el comando **cat** ("concatenate") que muestra el archivo completo en lugar de una página cada vez. También puede usarse para combinar varios archivos en uno solo. Por ejemplo::

	$cat archivo1 archivo2 archivo3 > gran_archivo.

Si omitimos *>gran_archivo* el resultado se escribirá en la pantalla y si emitimos los nombres antes de *>*, tomará la entrada de la pantalla hasa que el usuario presione <ctrl>-d.

Para ver las primeras líneas del documento usamos **head** y para ver las últimas, **tail**. Añadimos el número de líneas que queremos ver con -x, siendo x un número::

	$head -10 archivo
	$tail -10 archivo

Para eliminar un archivo usamos el comando **rm** ("remove"). Una buena precaución es usar *-i*, ya que obliga a *rm* a preguntar antes de eliminar dicho archivo. Se puede incluir esta opción automáticamente incluyendo la siguiente línea en el fichero de configuración de *bash*::

	alias rm='rm -i'

Para conseguir el efecto contrario, es decir, para forzar a eliminar los archivos sin preguntar usamos *-f* y para eliminar una carpeta con todo su contenido usamos *-r*.

Para mover (o renombrar) un archivo usamos el comando **mv** ("move") y para copiarlo **cp** ("copy"). Al igual que antes para mover o copiar una carpeta completa con su contenido usamo la opción *-r*.

Cuando ejecutamos un programa en la consola, si no especificamos nada, ésta quedará bloqueada hasta que dicho programa acabe de ejecutarse. Si queremos que se ejecute en *segundo plano* usamos **&**. Por ejemplo, si escribimos::

	$./programa &
	[1] 15442

Estaremos ejecutando el programa llamado programa que se halla en el directorio actual en segundo plano, lo que nos permite tener la consola libre para seguir trabajando. El output significa que es el trabajo número 1 que se ejecuta en segundo plano y cuyo *processor id.* es 15442. Para ver todos los trabajos en segundo plano que se están ejecutando en tu ordenador usamos::

	$jobs -l

Para volver a traer un trabajo a primer plano usamos **fg** (*foreground*)::

	$fg %1

Donde %1 indica que queremos traer el primer trabajo. Para devolverlo a segundo plano, usamos <ctrl>-z que detiene el programa y libera la consola. A continuación, usamos **bg** (*background*) para que se reanude::

	$bg %1

Si queremos detener un proceso usamos el comando **kill** junto con el *processor id.* En ocasiones, puede que esto no baste y por tanto, tendremos que añadir -9. Ojo, esto puede detener casi cualquier proceso, ten mucho cuidado. Un ejemplo de uso sería::

	$kill -9 15442

Hay ciertas acciones como instalar un paquete o un programa que no puede hacerlo un usuario normal, ya que implicaría una vulnerabilidad del sistema y por tanto, se requiere un privilegio de *superusuario*. Para ello usamos **sudo**::

	$sudo apt-get install paquete

En el ejemplo anterior, estamos usando nuestros privilegios de superusuario para instalar un paquete con el comando **apt-get install**.

Bash
----

[Información de Wikipedia] Bash es una shell de Unix escrito por Brian Fox en C para *GNU Project* como software libre. Lanzado en 1989 es la shell por defecto de Linux y OSX y ha sido llevado a Microsoft Windows y distribuido con Cygwin y MinGW. Bash es un procesador de comandos que se ejecuta normalmente en una terminal donde el usuario escribe los comandos, aunque también puede leer los comandos de un archivo, llamado *script*.
Nota: estamos asumiendo en todo momento que la shell que se usa es bash, si no fuera así, usa el comando **bash** para cambiarla.


Cuando iniciamos una nueva shell bash, por ejemplo, al abrir una consola, un archivo llamado ".bashrc" del directorio home se ejecuta como un script. En este archivo se puede colocar cualquier cosa que queramos que se ejecute cuando se inicialice como por ejemplo, cambiar el promt, exportar variables de entorno, establacer rutas...

Para cambiar el prompt hemos de modificar la variable PS1 de la siguiente manera::

	$PS1='nuevo_prompt '
	nuevo_prompt

Hay varias variables que se pueden usar para personalizar tu prompt, por ejemplo::

	$ PS1='[\W] \h% '
	[libro_prog] aire%

Nos informa que estoy en un directorio llamado "libro_prog" en un ordenador llamado "aire". Una vez que encuentres un promt a tu gusto, puedes poner este comando en ".bashrc".

Si queremos ver las variables de entorno predefinidas y las que hallamos creado, usamos **printenv**::

	$printenv
	USER=kirtash
	HOME=/home/kirtash
	UCACCAR=/home/ucaccar
	...
Para definir una variable, a una ruta usamos **export**, por ejemplo::

	$export UCACCAR=/home/ucaccar

Esto creará una variable cuya existencia será la misma que la terminal en la que halla sido creada, es decir, si se cerrase la consola y la volviésemos a abrir, tendríamos que volver a definir la variable, ya que esta se habría borrado. Para que esta variable se guarde y pueda usarse en repetidas ocasiones, debes copiar el comando en ".bashrc" para que todas las terminales definan dicha variable cada vez que se inicien.

Conectarse a un servidor remoto con SSH
========================================

[Aquí debe ir una introducción a ssh] **ssh** (*secure shell*) permite a un usuario conectarse a un ordenador de forma remota.

