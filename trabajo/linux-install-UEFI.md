UEFI
----

Antes de arrancar un sistema operativo, los ordenadores deben disponer (en su placa base) del software mínimo necesaro. Las placas base de los PC solían usar un estándar llamado BIOS. 

En los últimos años, este estándar se está reemplazando por uno nuevo llamado UEFI. Entre las características de UEFI, está "Secure Boot", una característica que, si está habilitada, inhabilita la posibilidad de activar el arranque de un sistema operativo diferente al preinstalado (usualmente Windows).

Por tanto, antes de habilitar doble arranque Linux+Windows (es decir, un sistema para decidir qué sistema operativo queremos arrancar cuando iniciemos el ordenador) habrá que:

1.- Investigar si el ordenador usa UEFI
2.- En caso afirmativo, entrar en UEFI y desactivar "Secure Boot" (si es que está activado)

A partir de ese momento, podemos instalar Linux y Windows, de forma que cohabiten en distintas particiones, mediante la forma usual.

Más información:

- What is UEFI: http://www.howtogeek.com/175649/what-you-need-to-know-about-using-uefi-instead-of-the-bios/

- How tocheck in Windows if you are using UEFI: http://blogs.technet.com/b/home_is_where_i_lay_my_head/archive/2012/10/02/how-to-check-in-windows-if-you-are-using-uefi.aspx   

- Windows 8 y Buntu en ordenadores con UEFI: http://itsfoss.com/install-ubuntu-1404-dual-boot-mode-windows-8-81-uefi/
